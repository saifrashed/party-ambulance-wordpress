<?php

defined('ABSPATH') || exit;

/**
 * Popup page Template
 *
 * Template Name:  Popup Pagina
 */

?>
<!doctype html>
<!--[if !IE]>
<html class="no-js non-ie" <?php language_attributes(); ?>> <![endif]-->
<!--[if IE 7 ]>
<html class="no-js ie7" <?php language_attributes(); ?>> <![endif]-->
<!--[if IE 8 ]>
<html class="no-js ie8" <?php language_attributes(); ?>> <![endif]-->
<!--[if IE 9 ]>
<html class="no-js ie9" <?php language_attributes(); ?>> <![endif]-->
<!--[if gt IE 9]><!-->
<html class="no-js" <?php language_attributes(); ?>> <!--<![endif]-->
<head>

    <meta charset="<?php bloginfo('charset'); ?>"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <title><?php wp_title('&#124;', true, 'right'); ?></title>

    <link rel="profile" href="http://gmpg.org/xfn/11"/>
    <link rel="pingback" href="<?php bloginfo('pingback_url'); ?>"/>

    <?php wp_head(); ?>
</head>
<body <?php body_class(); ?>>
<div id="container" class="hfeed">
    <div id="wrapper" class="clearfix">
        <div id="content-full" class="grid col-940">
            <div id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
                <div class="post-entry">
                    <?php the_content(); ?>
                </div>
            </div>
        </div><!-- end of #content-full -->
    </div>
</div>
<?php wp_footer(); ?>
</body>
</html>
    
    

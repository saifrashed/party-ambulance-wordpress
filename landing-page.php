<?php

defined('ABSPATH') || exit;

/**
 * Full Content Template
 *
 * Template Name:  Landingspagina (zonder sidebar)
 */

get_header();

?>
    <div id="content-full" class="grid col-940">
        <div id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
            <div class="post-entry">
                <?php

                the_content();

                $sizes          = array(
                    1 => 'col-940',
                    2 => 'col-460',
                    3 => 'col-300',
                    4 => 'col-220',
                    6 => 'col-140',
                );
                $aantal_columns = intval(get_post_meta(get_the_ID(), 'AantalKolommenOpLandingsPagina', true));
                $landingclass   = $sizes[$aantal_columns] ?? 'col-220';

                $ParentPageId = get_post_meta(get_the_ID(), 'ParentPageId', true);
                if (empty($ParentPageId) || $ParentPageId == -1) {
                    $ParentPageId = 0;
                }

                $landingquery = new WP_Query(array(
                    'post_parent'    => $ParentPageId,
                    'posts_per_page' => -1,
                    'post_type'      => 'page',
                    'meta_key'       => 'meta-text-titlelandingpage',
                    'meta_compare'   => '!=',
                    'meta_value'     => '',
                    'orderby'        => 'menu_order',
                    'order'          => 'ASC'
                ));

                if ($landingquery->have_posts()) {

                    echo '<div class="landinglist grid col-940">';

                    $cntlanding = 0;
                    while ($landingquery->have_posts()) {
                        $landingquery->the_post();
                        $title = get_post_meta(get_the_ID(), 'meta-text-titlelandingpage', true);
                        $thumb = get_the_post_thumbnail(get_the_ID(), 'large');
                        if (strlen($title) > 0 && strlen($thumb) > 0) {
                            $cntlanding++;
                            $fitclass = '';
                            if ($cntlanding % $aantal_columns == 0) {
                                $fitclass = ' fit';
                            }
                            echo '<div class="grid ' . $landingclass . $fitclass . '">';
                            echo '<h2><a href="' . get_permalink(get_the_ID()) . '">' . $title . '</a></h2>';
                            echo $thumb;


                            $argschild         = array(
                                'post_parent'    => get_the_ID(),
                                'post_type'      => 'page',
                                'posts_per_page' => -1,
                                'meta_key'       => 'meta-text-titlelandingpage',
                                'meta_compare'   => '!=',
                                'meta_value'     => '',
                                'orderby'        => 'menu_order',
                                'order'          => 'ASC'
                            );
                            $landingchildquery = new WP_Query($argschild);
                            if ($landingchildquery->have_posts()) {
                                // echo strlen(get_post_meta(get_the_ID(), 'meta-text-titlelandingpage', true));
                                echo '<div class="landingitems"><ul class="doormatlisting">';
                                while ($landingchildquery->have_posts()) {
                                    $landingchildquery->the_post();
                                    $itemtitle = get_post_meta(get_the_ID(), 'meta-text-titlelandingpage', true);
                                    if (strlen($itemtitle) == 0) {
                                        $itemtitle = get_the_title(get_the_ID());
                                    }
                                    echo '<li>';
                                    echo '<a href="' . get_permalink(get_the_ID()) . '">' . $itemtitle . '</a>';
                                    echo '</li>';
                                }
                                echo '</ul></div>';
                            }
                            echo '</div>';
                            if ($cntlanding % $aantal_columns == 0) {
                                echo '</div><div class="landinglist grid col-940">';
                            }
                        }
                    }
                    echo '</div>';
                }

                /*vind alle child pagina's van root node met een meta TitelOpLandingsPagina*/

                /*Loop door deze pagina's heen*/
                /*Toon afbeelding plus link en titel*/
                /*Kijk of er child pagina's onder deze pagina zijn met een meta TitelOpLandingsPagina*/
                /*Toon link met naam onder afbeelding*/

                ?>
            </div>
        </div>
    </div><!-- end of #content-full -->

<?php

get_footer();


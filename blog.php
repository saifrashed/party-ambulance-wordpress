<?php

defined('ABSPATH') || exit;

/**
 * Blog Template
 *
 * Template Name: Blog pagina (volledig)
 */

get_header();

global $more;
$more = 0;

?>
    <div id="content-blog" class="<?php echo implode(' ', responsive_get_content_classes()); ?>">
        <?php get_template_part('loop-header'); ?>
        <?php
        //echo 'pipo';
        the_content();
        ?>
        <?php
        global $wp_query, $paged;
        if (get_query_var('paged')) {
            $paged = get_query_var('paged');
        } elseif (get_query_var('page')) {
            $paged = get_query_var('page');
        } else {
            $paged = 1;
        }
        $blog_query = new WP_Query(array('post_type' => 'post', 'paged' => $paged));
        $temp_query = $wp_query;
        $wp_query   = null;
        $wp_query   = $blog_query;

        if ($blog_query->have_posts()) :

            while ($blog_query->have_posts()) : $blog_query->the_post();
                ?>
                <?php $postclass = (++$j % 2 == 0) ? 'evenpost' : 'oddpost'; ?>
                <div id="post-<?php the_ID(); ?>" <?php post_class($postclass); ?>>
                    <div class="post-title">
                        <?php get_template_part('post-meta'); ?>
                    </div>
                    <?php
                    $page_id  = get_the_ID();
                    $hasimage = has_post_thumbnail($page_id);
                    if ($hasimage) {
                        echo '<div class="post-image">';
                        echo get_the_post_thumbnail($page_id, 'large', array('class' => 'aligncenter'));
                        echo '</div>';
                    }
                    ?>
                    <?php $postentryclass = $hasimage ? 'image-width' : 'full-width'; ?>
                    <div class="post-entry <?php echo $postentryclass; ?>">


                        <?php the_content(__('Read more &#8250;', 'responsive')); ?>

                        <?php wp_link_pages(array('before' => '<div class="pagination">' . __('Pages:', 'responsive'), 'after' => '</div>')); ?>
                    </div>
                    <!-- end of .post-entry -->

                    <?php get_template_part('post-data'); ?>

                </div><!-- end of #post-<?php the_ID(); ?> -->


            <?php
            endwhile;

            get_template_part('loop-nav');

        else :

            get_template_part('loop-no-posts');

        endif;

        wp_reset_postdata();

        $wp_query = $temp_query;
        ?>

    </div><!-- end of #content-blog -->

<?php

get_sidebar();

get_footer();


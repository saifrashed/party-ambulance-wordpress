<?php

defined('ABSPATH') || exit;

/**
 * Archive Template
 */

get_header();

?>

    <div id="content-archive" class="grid col-940">

        <?php if (have_posts()) : ?>

            <?php get_template_part('loop-header'); ?>

            <?php while (have_posts()) : the_post(); ?>

                <?php $postclass = (++$j % 2 == 0) ? 'evenpost' : 'oddpost'; ?>
                <div id="post-<?php the_ID(); ?>" <?php post_class($postclass); ?>>
                    <div class="post-title">
                        <?php get_template_part('post-meta'); ?>
                    </div>


                    <?php
                    $page_id  = get_the_ID();
                    $hasimage = has_post_thumbnail($page_id);
                    if ($hasimage) {
                        echo '<div class="post-image">';
                        echo get_the_post_thumbnail($page_id, 'large', array('class' => 'aligncenter'));
                        echo '</div>';
                    }
                    ?>
                    <?php $postentryclass = $hasimage ? 'image-width' : 'full-width'; ?>
                    <div class="post-entry <?php echo $postentryclass; ?>">


                        <?php the_excerpt(); ?>

                        <?php wp_link_pages(array('before' => '<div class="pagination">' . __('Pages:', 'responsive'), 'after' => '</div>')); ?>
                    </div>
                    <!-- end of .post-entry -->

                    <?php get_template_part('post-data'); ?>


                </div><!-- end of #post-<?php the_ID(); ?> -->


            <?php
            endwhile;

            get_template_part('loop-nav');

        else :

            get_template_part('loop-no-posts');

        endif;
        ?>

    </div><!-- end of #content-archive -->

<?php

get_sidebar();

get_footer();



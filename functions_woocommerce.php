<?php

defined('ABSPATH') || exit;

/**
 * Functions woocommerce
 */
/**
 * Woocommerce
 *
 *
 */
/**
 * Change In Stock / Out of Stock Text EDIT BY SVEN
 */
add_filter('woocommerce_get_availability', 'wcs_custom_get_availability', 1, 2);

/** Display if in stock or not */
function wcs_custom_get_availability($availability, $_product) {

    // Change In Stock Text
    if ($_product->is_in_stock()) {
        $availability['availability'] = __('Op voorraad.', 'woocommerce');
    }
    // Change Out of Stock Text
    if (!$_product->is_in_stock()) {
        $availability['availability'] = __('Uitverkocht', 'woocommerce');
    }
    return $availability;
}

function sv_remove_product_page_skus($enabled) {
    if (!is_admin() && is_product()) {
        return false;
    }
    return $enabled;
}

add_filter('wc_product_sku_enabled', 'sv_remove_product_page_skus');


add_action('init', 'woocommerce_actions_add_and_remove');

function woocommerce_actions_add_and_remove() {
//    remove_action( 'woocommerce_before_single_product', 'wc_print_notices', 10 );
    remove_action('woocommerce_before_main_content', 'responsive_pro_woocommerce_wrapper', 10);
    remove_action('woocommerce_after_main_content', 'responsive_pro_woocommerce_wrapper_end', 10);
    //  remove_action('woocommerce_single_product_summary', 'woocommerce_template_single_excerpt', 20);
//    add_action('woocommerce_single_product_summary', 'wc_print_notices', 10);
//    remove_action('woocommerce_single_product_summary', 'woocommerce_template_single_meta', 40);
//    remove_action('woocommerce_single_product_summary', 'woocommerce_template_single_sharing', 50);
//    remove_action('woocommerce_single_product_summary', 'WC_Structured_Data::generate_product_data', 60);
    remove_action('woocommerce_single_product_summary', 'woocommerce_template_single_price', 10);
//    remove_action('woocommerce_before_add_to_cart_form', 'woocommerce_template_single_price', 10);
//    remove_action( 'woocommerce_single_variation', 'woocommerce_single_variation', 10 );
//    remove_action('woocommerce_cart_collaterals', 'woocommerce_cross_sell_display', 10);
    //  add_action('woocommerce_single_product_summary', 'woocommerce_template_single_excerpt', 60);

    //title
    //remove_action('woocommerce_single_product_summary', 'woocommerce_template_single_title', 5);
    //remove_action('woocommerce_single_product_summary', 'woocommerce_template_single_rating', 10);

    //remove_action('woocommerce_single_product_summary', 'woocommerce_template_single_excerpt', 20);
    //remove_action('woocommerce_single_product_summary', 'woocommerce_template_single_add_to_cart', 30);

    // add_action('woocommerce_after_variations_form', 'woocommerce_template_single_price', 40);
    add_action('woocommerce_before_add_to_cart_quantity', 'woocommerce_template_single_price', 40);
//    remove_action('woocommerce_single_variation','woocommerce_single_variation_add_to_cart_button' ,20);
//
//    add_action('woocommerce_after_variations_form', 'woocommerce_single_variation', 40);
//    add_action('woocommerce_after_variations_form', 'woocommerce_single_variation_add_to_cart_button', 50);
    // add_action('woocommerce_single_variation','woocommerce_single_variation_add_to_cart_button' ,5);


    /**
     * Hook: woocommerce_single_product_summary.
     *
     * @hooked woocommerce_template_single_title - 5
     * @hooked woocommerce_template_single_rating - 10
     * @hooked woocommerce_template_single_price - 10
     * @hooked woocommerce_template_single_excerpt - 20
     * @hooked woocommerce_template_single_add_to_cart - 30
     * @hooked woocommerce_template_single_meta - 40
     * @hooked woocommerce_template_single_sharing - 50
     * @hooked WC_Structured_Data::generate_product_data() - 60
     */
}

///** Remove the product rating display on product loops */
//remove_action( 'woocommerce_after_shop_loop_item_title', 'woocommerce_template_loop_rating', 5 );
//
///** Remove categories from product page */
//remove_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_meta', 40 );
//
///** Remove the sorting dropdown from Woocommerce */
////remove_action( 'woocommerce_before_shop_loop' , 'woocommerce_catalog_ordering', 30 );
//
///** Remove the result count from WooCommerce */
//remove_action( 'woocommerce_before_shop_loop' , 'woocommerce_result_count', 20 );
//
///** Hide category product count */
//add_filter( 'woocommerce_subcategory_count_html', '__return_false' );

//
///** Winkelmandje button single products */
//add_filter( 'add_to_cart_text', 'woo_custom_single_add_to_cart_text' );
//add_filter( 'woocommerce_product_single_add_to_cart_text', 'woo_custom_single_add_to_cart_text' );
//function woo_custom_single_add_to_cart_text() {
//    return __( 'In winkelmandje', 'responsive' );
//}
//
///** Text of relatable products */
//add_filter('woocommerce_product_add_to_cart_text', 'woo_archive_custom_cart_button_text');
//function woo_archive_custom_cart_button_text() {
//    return __('Selecteer', 'responsive');
//}
//
///** Remove the additional information tab */
//add_filter( 'woocommerce_product_tabs', 'woo_remove_product_tabs', 98 );
//function woo_remove_product_tabs( $tabs ) {
//    unset( $tabs['additional_information'] );
//    return $tabs;
//}

/** Remove all currency symbols */
//function sww_remove_wc_currency_symbols( $currency_symbol, $currency ) {
//    $currency_symbol = '';
//    return $currency_symbol;
//}
//add_filter('woocommerce_currency_symbol', 'sww_remove_wc_currency_symbols', 10, 2);


/** Add text to end of prices */
//function cw_change_product_price_display( $price ) {
//    $price .= ',-';
//    return $price;
//}
//add_filter( 'woocommerce_get_price_html', 'cw_change_product_price_display' );
//add_filter( 'woocommerce_cart_item_price', 'cw_change_product_price_display' );

/** Remove single post 'added to cart' message */
//add_filter( 'wc_add_to_cart_message_html', 'empty_wc_add_to_cart_message');
//function empty_wc_add_to_cart_message() {
//    return '';
//};

/** Change number or products per row */
add_filter('loop_shop_columns', 'loop_columns');
if (!function_exists('loop_columns')) {
    function loop_columns() {
        return 3; // 3 products per row
    }
}

/**
 * Add icon and price in add to cart
 */

add_filter('woocommerce_product_add_to_cart_text', 'change_text_woo');
function change_text_woo() {
    global $product;

    $price       = $product->get_price_html();
    $button_text = '<div class="product-button"><i class="fa fa-cart-plus"></i>' . $price . '</div>';

    if ( $product->is_type( 'variable' ) ) {
        $button_text = '<div class="product-button">' . $price . '</div>';

    }


    return $button_text;
}


/**
 *
 * Remove default prices in the loop
 *
 */

add_filter('woocommerce_after_shop_loop_item_title', 'remove_woocommerce_loop_price', 2);
function remove_woocommerce_loop_price() {
    remove_action('woocommerce_after_shop_loop_item_title', 'woocommerce_template_loop_price', 10);
}

/** If product doesnt have set price or price is 0 given text will display */
add_filter('woocommerce_get_price_html', 'bbloomer_price_free_zero_empty', 100, 2);

function bbloomer_price_free_zero_empty($price, $product) {

    if ('' === $product->get_price() || 0 == $product->get_price()) {
        $price = '<span class="woocommerce-Price-amount amount"><span class="woocommerce-Price-currencySymbol">€</span>0</span>';
    }

    return $price;
}

/** Facetwp count output */
add_filter('facetwp_result_count', function ($output, $params) {
    $output = $params['lower'] . '-' . $params['upper'] . ' of ' . $params['total'] . ' resultaten';
    return $output;
}, 10, 2);

// Remove the tabs from woocommerce - posted by Robin Scott of Silicon Dales @ https://silicondales.com/tutorials/woocommerce-tutorials/remove-tab-woocommerce/
add_filter( 'woocommerce_product_tabs', 'woo_remove_product_tabs', 98 );
function woo_remove_product_tabs( $tabs ) {
    unset( $tabs['description'] ); // Remove the description tab
    unset( $tabs['reviews'] ); // Remove the reviews tab
    unset( $tabs['additional_information'] ); // Remove the additional information tab
    return $tabs;
}

/* Remove product meta */
remove_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_meta', 40 );

add_theme_support('wc-product-gallery-zoom');
add_theme_support('wc-product-gallery-lightbox');
add_theme_support('wc-product-gallery-slider');

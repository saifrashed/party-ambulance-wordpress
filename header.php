<?php

defined('ABSPATH') || exit;

/**
 * Header Template
 */

?>
<!doctype html>
<!--[if !IE]>
<html class="no-js non-ie" <?php language_attributes(); ?>> <![endif]-->
<!--[if IE 7 ]>
<html class="no-js ie7" <?php language_attributes(); ?>> <![endif]-->
<!--[if IE 8 ]>
<html class="no-js ie8" <?php language_attributes(); ?>> <![endif]-->
<!--[if IE 9 ]>
<html class="no-js ie9" <?php language_attributes(); ?>> <![endif]-->
<!--[if gt IE 9]><!-->
<html class="no-js" <?php language_attributes(); ?>> <!--<![endif]-->
<head>
    <meta charset="<?php bloginfo('charset'); ?>"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="pingback" href="<?php bloginfo('pingback_url'); ?>"/>
    <?php wp_head(); ?>
</head>
<body <?php body_class(); ?>>
<?php responsive_container(); // before container hook  ?>

<div class="headertoplogo">
    <img src="/wp-content/uploads/2019/01/logo.png" />
</div>

<?php if (has_nav_menu('top-menu')) { ?>
    <div class="headertopcontainer">
        <div id="container"><?php
            wp_nav_menu(array(
                    'container'      => '',
                    'fallback_cb'    => false,
                    'menu_class'     => 'top-menu',
                    'theme_location' => 'top-menu'
                )
            ); ?>
        </div>
    </div>
<?php } ?>
<div class="headermenucontainer">
    <div id="container" class="hfeed">
        <?php if (is_multisite() && strpos(get_option('multisite_menu_names'), 'sub-header-menu') !== false) {
            $show_subheader_menu = get_site_transient('wds_sub-header-menu');
            echo $show_subheader_menu;
        } else {
            if (has_nav_menu('sub-header-menu')) {
                wp_nav_menu(array(
                        'container'       => 'div',
                        'container_class' => 'main-nav',
                        'fallback_cb'     => 'responsive_fallback_menu',
                        'theme_location'  => 'sub-header-menu'
                    )
                );
            }
        } ?>

        <?php responsive_header_bottom(); // after header content hook ?>
        <!--        <div class="headersearch">
            <?php // get_search_form(); ?>
        </div>-->
    </div><!-- end of #header -->
</div>

<?php

responsive_header_end(); // after header container hook
responsive_wrapper(); // before wrapper container hook
responsive_wrapper_top(); // before wrapper content hook
responsive_in_wrapper(); // wrapper hook

if (is_page() || is_tax() || is_singular() || is_404() || is_search()) {
    $frontpage_id = get_option('page_on_front');
    $slidename    = get_post_meta(get_the_ID(), 'meta-select-slider', true);

    if (strlen($slidename) === 0) {
        if (is_tax()) {
            $term = get_queried_object();
            $tax_field = get_field('cyclone_slider', $term->taxonomy . '_' . $term->term_id);
            if ($tax_field) {
                $slidename = $tax_field;
            }
        }

        if (is_singular() && 'product' == get_post_type()) {
            $slidename = 'webshop';
        }

        if (strlen($slidename) === 0) {
            $frontpage_id = get_option('page_on_front');
            if ($frontpage_id > 0) {
                $slidename = get_post_meta($frontpage_id, 'meta-select-slider', true);
            }

        }

    }

    if (function_exists('cyclone_slider') && strlen($slidename) > 0 && $slidename != 'noslider') {
        cyclone_slider($slidename);
    }
}

?>
<div id="fullcontainer" class="fixslider">
    <div id="container" class="hfeed">
        <div id="wrapper" class="clearfix">
            
            
            

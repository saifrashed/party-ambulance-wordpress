<?php

defined('ABSPATH') || exit;

/**
 * Archive Template
 */

get_header();

?>
    <div id="content-category" class="<?php echo implode(' ', responsive_get_content_classes()); ?>">
        <?php
        if (have_posts()) :

            get_template_part('loop-header');

            global $hor_backgroundimage;

            // Run hor_blocks
            echo hor_shortcode('hor_blocks', array(
                'id'              => 'category',
                'layout'          => 'blog',
                'backgroundimage' => $hor_backgroundimage,
                'load_method'     => 'pagination',
                'query'           => true,
            ));

        else :

            get_template_part('loop-no-posts');

        endif;
        ?>
    </div><!-- end of #content-archive -->
<?php

get_sidebar();

get_footer();


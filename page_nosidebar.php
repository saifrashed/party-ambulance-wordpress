<?php

defined('ABSPATH') || exit;

/**
 * Pages Template
 *
 * Template Name:  Standaardpagina (zonder sidebar)
 */

get_header();

?>
    <div id="content-full" class="grid col-940">
        <?php get_responsive_breadcrumb_lists(); ?>
        <div id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
            <div class="post-entry">
                <?php the_content(); ?>
            </div>
        </div>
    </div><!-- end of #content-full -->
<?php

get_footer();

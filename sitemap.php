<?php

defined('ABSPATH') || exit;

/**
 * Sitemap Template
 *
 * Template Name:  Sitemap Pagina
 */

get_header();

?>
    <div id="content-full" class="grid col-620">
        <?php get_responsive_breadcrumb_lists(); ?>
        <div id="post-//<?php the_ID(); ?>" <?php post_class(); ?>>
            <div class="post-entry">
                <?php the_content(); ?>
                <!-- Site Pages -->
                <?php

                $excludeposttype      = get_option('hide_posttype_sitemap');
                $excludeposttypeArray = explode(',', $excludeposttype);
                $post_types           = get_post_types(array('public' => true), 'objects');
                foreach ($post_types as $post_type) {
                    if (!in_array($post_type->name, $excludeposttypeArray)) {
                        $hideposttypeids = array($excludeposttype);
                        $hidept_args     = array(
                            'post_type'      => $post_type->name,
                            'posts_per_page' => -1,
                            'meta_key'       => 'meta-checkbox-hideinsitemap',
                            'meta_compare'   => '=',
                            'meta_value'     => 'yes'
                        );
                        $hideposttypes   = get_posts($hidept_args);
                        if ($hideposttypes) {
                            foreach ($hideposttypes as $hideposttypes) {
                                $hideposttypeids[] = $hideposttypes->ID;
                            }
                        }
                        $pt_args  = array(
                            'post_type'      => $post_type->name,
                            'posts_per_page' => -1,
                            'exclude'        => implode(",", $hideposttypeids),
                            'post_status'    => 'publish',
                            'orderby'        => 'menu_order, post_title',
                            'order'          => 'ASC'
                        );
                        $pt_posts = get_posts($pt_args);
                        if (count($pt_posts) > 0) {
                            echo '<div id="sitemaposts"><h2>' . $post_type->label . '</h2><ul class="list-unstyled">';
                            foreach ($pt_posts as $p) {
                                echo '<li><a href="' . get_permalink($p) . '">' . get_the_title($p) . '</a></li>';
                            }
                            echo '</div></ul>';
                        }
                    }
                }
                ?>
            </div>
        </div>
    </div><!-- end of #content-full -->
<?php

get_sidebar('right');

get_footer();

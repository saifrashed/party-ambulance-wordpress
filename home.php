<?php

defined('ABSPATH') || exit;

/**
 * Blog Template
 */

get_header();

global $more;
$more = 0;

?>
    <div id="content-blog" class="<?php echo esc_attr(implode(' ', responsive_get_content_classes())); ?>">
        <?php get_template_part('loop-header'); ?>
        <!-- Blog page title -->
        <?php if (responsive_pro_get_option('blog_post_title_toggle')) { ?>
            <h1> <?php echo responsive_pro_get_option('blog_post_title_text'); ?> </h1>
        <?php } ?>

        <?php if (have_posts()) : ?>

            <?php while (have_posts()) : the_post(); ?>

                <?php responsive_entry_before(); ?>
                <?php $postclass = (++$j % 2 == 0) ? 'evenpost' : 'oddpost'; ?>
                <div id="post-<?php the_ID(); ?>" <?php post_class($postclass); ?>>
                    <h2 class="entry-title post-title"><a href="<?php the_permalink() ?>"
                                                          rel="bookmark"><?php the_title(); ?></a></h2>
                    <?php responsive_entry_top(); ?>
                    <?php
                    $page_id        = get_the_ID();
                    $fullwidthclass = 'fullwidth';
                    if (has_post_thumbnail($page_id) || has_post_thumbnail($post_id)) {
                        $fullwidthclass = '';

                        ?>
                        <div class="post-image">
                            <?php
                            if (has_post_thumbnail($page_id)) {
                                echo get_the_post_thumbnail($page_id, 'medium', array('class' => 'alignleft'));
                            } else {
                                echo get_the_post_thumbnail($post_id, 'medium', array('class' => 'alignleft'));
                            }
                            ?>
                        </div>

                    <?php } ?>
                    <div class="post-entry <?php echo $fullwidthclass ?>">
                        <div class="post-meta">
                            <?php
                            responsive_pro_posted_on();
                            responsive_pro_posted_by();
                            responsive_pro_comments_link();
                            ?>
                        </div><!-- end of .post-meta -->

                        <?php
                        if (responsive_pro_get_option('blog_post_excerpts')) {
                            add_filter('excerpt_more', 'responsive_pro_excerpt_more_text');
                            add_filter('excerpt_length', 'responsive_pro_excerpt_more_length');
                            the_excerpt();
                            remove_filter('excerpt_more', 'responsive_pro_excerpt_more_text');
                            remove_filter('excerpt_length', 'responsive_pro_excerpt_more_length');
                        } else {
                            the_content(__('Read more &#8250;', 'responsive'));
                        }

                        wp_link_pages(array('before' => '<div class="pagination">' . __('Pages:', 'responsive'), 'after' => '</div>'));
                        ?>
                    </div>
                    <!-- end of .post-entry -->


                    <?php responsive_entry_bottom(); ?>
                </div><!-- end of #post-<?php the_ID(); ?> -->
                <?php responsive_entry_after(); ?>

            <?php
            endwhile;

            if ($wp_query->max_num_pages > 1) :
                ?>
                <div class="navigation">
                    <div class="previous"><?php next_posts_link(__('&#8249; Older posts', 'responsive'), $wp_query->max_num_pages); ?></div>
                    <div class="next"><?php previous_posts_link(__('Newer posts &#8250;', 'responsive'), $wp_query->max_num_pages); ?></div>
                </div><!-- end of .navigation -->
            <?php
            endif;

        else :

            get_template_part('loop-no-posts');

        endif;
        ?>

    </div><!-- end of #content-blog -->

<?php

get_sidebar();

get_footer();
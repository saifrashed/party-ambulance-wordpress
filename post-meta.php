<?php

defined('ABSPATH') || exit;

/**
 * Post Meta-Data Template-Part File
 */

if (is_single()) {
    if (strpos(get_the_content(), '</h1>') === false) {
        echo '<h1 class="entry-title post-title">' . get_the_title() . '</h1>';
    }
} else {
    echo '<h2 class="entry-title post-title"><a href="' . get_the_permalink() . '" rel="bookmark">' . get_the_title() . '</a></h2>';
}

if (get_post_type() === 'post') {

    ?>
    <div class="post-meta">
        <?php

        responsive_pro_posted_on();
        responsive_pro_posted_by();
        responsive_pro_comments_link();

        ?>
    </div><!-- end .post-meta -->
    <?php
}

(function ($) {
    $(function () {

        // search
        window.checksearchinput = function (form) {
            var f = $(form);
            if (f.parent().hasClass('headersearch')) {
                var input = f.parent().find('.inputsearch_area');
                if (input && input.is(':visible')) {
                    if (form.elements["s"].value != '') {
                        return true;
                    }
                    input.hide();
                    return false;
                }
                input.show('slow');
                // Focus on input element
                input.find('input').focus();
                return false;
            }
            if (form.elements["s"].value != '') {
                return true;
            }
            return false;
        };

        var p = $('p.login-password');
        var u = $('p.login-username');
        if (p.length > 0 && u.length > 0) {
            p.children('input').attr('placeholder', p.children('label').text());
            u.children('input').attr('placeholder', u.children('label').text());
        }

        // https://github.com/woothemes/FlexSlider/wiki/FlexSlider-Properties
        var flexslider = $('.flexslider');
        flexslider.flexslider({
            animation: "fade",
            slideshowSpeed: 7000,
            slideshow: true,
            controlNav: true,
            prevText: '',
            nextText: '',
            directionNav: true
        });
        flexslider.find('.flex-prev').addClass('fa fa-3x fa-angle-left');
        flexslider.find('.flex-next').addClass('fa fa-3x fa-angle-right');

        // $('a[href*=\\#]:not([href=\\#])').click(function () {
        //     if (location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '') && location.hostname == this.hostname) {
        //         if (this.hash.indexOf('#ac_') == -1) { //disable for faq plugin
        //             var target = $(this.hash);
        //             target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
        //             if (target.length) {
        //                 var scrollpos = target.offset().top - 175;
        //                 $('html,body').animate({
        //                     scrollTop: scrollpos
        //                 }, 1000);
        //                 return false;
        //             }
        //         }
        //     }
        // });

        $('.headersearch .inputsearch_area').hide();
        // disable scrolling over a google map.
        $('.wpb_gmaps_widget iframe').addClass('scrolloff');
        $('.wpb_gmaps_widget').on('click', function () {
            $('.wpb_gmaps_widget iframe').removeClass('scrolloff');
        });
        $('.wpb_gmaps_widget').mouseleave(function () {
            $('.wpb_gmaps_widget iframe').addClass('scrolloff');
        });
        // formidable tooltip to display message.
        $('.frm_tooltip_icon').click(function () {
            if ($(this).html().length == 0) {
                $(this).html(' ' + $(this).attr('title'));
            } else {
                $(this).html('');
            }
        });

        // Methode om ervoor te zorgen dat de gecachde multisite menu's gecheckt worden op active states.
        $('.main-nav > .menu  li > a').each(function () {

            var href = $(this).attr('href');
            $(this).parent().removeClass('current_page_item current-menu-item current-menu-ancestor current-menu-parent current_page_parent current_page_ancestor');

            if (href.slice(-1) != '/') {
                href += '/';
            }

            if (href == window.location || href == window.location.pathname) {
                $(this).parent().addClass('current_page_item').addClass('current-menu-item');
            }
        });

        $('.main-nav > .menu > li').each(function () {
            if ($(this).hasClass('current-menu-item')) {
                return;
            }
            if ($(this).find('li').hasClass('current-menu-item')) {
                $(this).addClass('current_page_item current-menu-item current-menu-ancestor current-menu-parent current_page_parent current_page_ancestor')
            }
        });

        // The header menu container
        var header = $('.headermenucontainer');

        // window height
        var window_height = $(window).height();

        // window width
        var window_width = $(window).width();

        // totop
        var totop = $('.toTop');

        totop.hide().css({
            'position': 'fixed',
            'bottom': 20,
            'right': 20,
            'z-index': 99
        });

        header.css({
            'z-index': 9999,
            'top': 0,
            'width': '100%',
        });

        // main nav
        var main_nav = $('.main-nav');

        // logo
        var logo = $('#logo');

        // grab the initial top offset of the navigation 
        var sticky_navigation_offset_top = header.offset().top;

        // our function that decides weather the navigation bar should have "fixed" css position or not.
        var sticky_navigation = function () {

            var scroll_top = !isNaN(window.scrollY) ? window.scrollY : $(window).scrollTop(); // our current vertical position from the top

            // if we've scrolled more than the navigation, change its position to fixed to stick to top, otherwise change it back to relative
            var scroll_gt_offset = scroll_top > sticky_navigation_offset_top;

            if (window_width > 650 && scroll_gt_offset) {
                header.css('position', 'fixed');
                $('body .cycloneslider').css('margin-top', '65px');
            } else {
                header.css('position', 'relative');
                $('body .cycloneslider').css('margin-top', '0px');
                console.log('hello');
            }

            main_nav.toggleClass('main-navcontainer', scroll_gt_offset);
            logo.toggleClass('small-logo', scroll_gt_offset);

            if (scroll_top > window_height) {
                totop[0] ? totop[0].style.display = 'block' : 0;
            } else {
                totop[0] ? totop[0].style.display = 'none' : 0;
            }
        };

        // run our function on load
        sticky_navigation();

        // and run it again every time you scroll
        $(window).scroll(sticky_navigation);

    });


    // Facetwp reset filters button activation
    $(".reset-filters").click(function (e) {
        FWP.reset();
    });

    // Facetwp reset filters button hidden when no filters are used
    var changeNot = false;
    $('.facetwp-selections').on("DOMSubtreeModified", function () {
        if (!changeNot) {
            $('.facetwp-selections li [data-value=""]').each(function (e) {
                changeNot = true;
                var newHTML = $(this).html();
                $(this).html(newHTML);
            });
            changeNot = false;
            if (document.location.search == "") {
                $('.reset-filters').css({"visibility": "hidden"});
            } else {
                $('.reset-filters').css({"visibility": "visible"});
            }
        }
    });

    $('input.facetwp-slider-reset').css('visibility', 'hidden');

})(jQuery);
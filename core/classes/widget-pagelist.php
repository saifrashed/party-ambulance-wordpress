<?php

namespace BlueMammoth;

/**
 * Class WidgetPageList
 *
 * Refactored by Max Hoogenbosch :) (16-07-2018)
 *
 * @author Max Hoogenbosch
 */
class PageListWidget extends \WP_Widget {

    /**
     * Setup widget
     */
    public function __construct() {
        $this->textdomain = 'widgetpagelist';
        $widget_options   = array(
            'classname'   => 'pagelist',
            'description' => __('Snelmenu Widget', 'calltoaction'),
        );
        $control_options  = array(
            'width'   => 300,
            'height'  => 350,
            'id_base' => 'pagelist-widget',
        );
        parent::__construct('pagelist-widget', __('Snelmenu Widget', 'calltoaction'), $widget_options, $control_options);
    }


    /**
     * Display widget
     *
     * @param array $args
     * @param array $instance
     */
    public function widget($args, $instance) {
        if (!is_page()) {
            return;
        }
        echo $args['before_widget'];
        $this->display($instance);
        echo $args['after_widget'];
    }

    /**
     * Render widget
     *
     * @param $instance
     */
    public function display($instance) {
        $parentid  = 0;
        $menutitle = $instance['title'];

        if ($instance['hierarchie'] === 'on') {
            $depth = 0;
        } else {
            $menutitle = $this->parentMenuTitle(get_the_ID());
            $depth     = 0;
        }

        $args = array(
            'child_of'    => $parentid,
            'depth'       => $depth,
            'post_type'   => 'page',
            'post_status' => 'publish',
            'sort_column' => 'menu_order, post_title',
            'link_after'  => '',
            'link_before' => '',
            'title_li'    => '',
            'walker'      => '',
            'echo'        => false,
        );

        $hidepages = get_posts(array(
            'fields'         => 'ids',
            'posts_per_page' => -1,
            'post_type'      => 'page',
            'meta_key'       => 'meta-checkbox-hideinquickmenu',
            'meta_compare'   => '=',
            'meta_value'     => 'yes',
        ));

        if (is_array($hidepages) && count($hidepages) > 0) {
            $args['exclude'] = implode(',', $hidepages);
        }

        $html = apply_filters('the_content', $instance['text']);
        $list = wp_list_pages($args);

        echo <<< HTML
        <div class="pagelistheader">{$html}</div>
        <div class="landingitems">
            <div class="quickmenu">$menutitle</div>
            <ul class="doormatlisting">$list</ul>
        </div>
HTML;
        wp_reset_postdata();
    }

    public function parentMenuTitle($pageID) {
        if ($pageID > 0) {
            $CurrentMenuTitle = get_post_meta($pageID, 'meta-text-titlelandingpage', true);
            if (strlen($CurrentMenuTitle) > 0) {
                return $pageID;
            }

            $post = get_post($pageID);
            if (isset($post->post_parent) && $post->post_parent > 0) {
                return $this->parentMenuTitle($post->post_parent);
            }
        }
        return 0;
    }

    /**
     * Widget options form
     *
     * @param array $instance
     * @return string|void
     */
    public function form($instance) {
        $defaults = array(
            'title'      => '',
            'text'       => '',
            'hierarchie' => true,
        );
        $instance = wp_parse_args($instance, $defaults);
        $f        = new F();
        echo <<< HTML
    <div>
        <label for="{$this->get_field_id('title')}">{$f->__('Snelmenu titel', 'title')}</label>
        <input type="text" maxlength="25" id="{$this->get_field_id('title')}"
               name="{$this->get_field_name('title')}" style="width:100%;"
               value="{$instance['title']}"/>
    </div>
    <div>
        <label for="{$this->get_field_id('hierarchie')}">{$f->__('Toon altijd alle pagina\'s?', 'hierarchie')}</label><br/>
        <input type="checkbox" id="{$this->get_field_id('hierarchie')}"
               name="{$this->get_field_name('hierarchie')}" {$f->checked($instance['hierarchie'], 'on', false)} />
    </div>
    <div>
        <label for="{$this->get_field_id('text')}">{$f->__('HTML Boven de paginalijst', 'example')}</label>
        <textarea id="{$this->get_field_id('text')}" rows="8"
                  name="{$this->get_field_name('text')}"
                  style="width:100%;">{$instance['text']}</textarea>
    </div>
HTML;
    }

    /**
     * Update Widget options
     *
     * @param array $new_instance
     * @param array $old_instance
     * @return array
     */
    public function update($new_instance, $old_instance) {
        return array_merge($old_instance, $new_instance);
    }

}

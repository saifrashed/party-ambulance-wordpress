<?php

namespace BlueMammoth;

/**
 * Object function caller
 *
 * Usefull for working with heredoc's
 *
 * @author  Max Hoogenbosch
 * @package BlueMammoth
 */
class F {

    /**
     * @param callable $name
     * @param array    ...$arguments
     * @return mixed
     */
    public function __call(callable $name, array $arguments = array()) {
        return call_user_func_array($name, $arguments);
    }

}
<?php

defined('ABSPATH') || exit;

/**
 * Search Template
 */

get_header();

$count = 0;

?>
    <div id="content-search" class="<?php echo esc_attr(implode(' ', responsive_get_content_classes())); ?>">
        <?php

        if (have_posts()) :

            get_template_part('loop-header');

            while (have_posts()) : the_post();

                $postclass = (++$count % 2 == 0) ? 'evenpost' : 'oddpost';

                ?>
                <div id="post-<?php the_ID(); ?>" <?php post_class($postclass); ?>>
                    <div class="post-title">
                        <?php get_template_part('post-meta'); ?>
                    </div>
                    <?php

                    $hasimage        = has_post_thumbnail();
                    $backgroundimage = $hor_backgroundimage;

                    if ($hasimage) {
                        $image = wp_get_attachment_image_src(get_post_thumbnail_id(), 'thumbnail');
                        if (count($image) > 0 && $image[0]) {
                            $backgroundimage = $image[0];
                        }
                    } else {
                        $homepageid = get_option('page_on_front');
                        if (has_post_thumbnail($homepageid)) {
                            $image = wp_get_attachment_image_src(get_post_thumbnail_id($homepageid), 'thumbnail');
                            if (count($image) > 0 && $image[0]) {
                                $backgroundimage = $image[0];
                            }
                        }
                    }

                    ?>
                    <div class="post-image">
                        <img class="alignleft" src="<?php echo $backgroundimage ?>"/>
                    </div>
                    <div class="post-entry image-width">
                        <?php

                        $tmpExcerpt   = strlen(trim($post->post_excerpt)) > 0 ? $post->post_excerpt : strip_shortcodes(wp_strip_all_tags($post->post_content));
                        $more_excerpt = setexcerptlength($tmpExcerpt, 50, '..');

                        if (strlen($more_excerpt) > 10) {
                            echo $more_excerpt;
                            echo '<div class="read-more"><a class="button" href="' . get_the_permalink() . '">' . __('Read more &#8250;', 'responsive') . '</a></div>';
                        } else {
                            the_excerpt();
                        }

                        ?>
                    </div>
                    <!-- end of .post-entry -->
                    <?php get_template_part('post-data'); ?>
                </div><!-- end of #post-<?php the_ID(); ?> -->

            <?php
            endwhile;

            get_template_part('loop-nav');

        else :

            get_template_part('loop-no-posts');

        endif;
        ?>
    </div><!-- end of #content-search -->
<?php

get_sidebar();

get_footer();

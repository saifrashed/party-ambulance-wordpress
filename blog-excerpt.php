<?php

defined('ABSPATH') || exit;

/**
 * Blog Template
 *
 * Template Name: Blog Samenvattingspagina
 */

get_header();

?>
    <div id="content-blog" class="<?php echo implode(' ', responsive_get_content_classes()); ?>">
        <div class="contentblogtext">
            <?php get_template_part('loop-header'); ?>
            <?php the_content(); ?>
        </div>
        <?php
        global $wp_query, $paged;
        if (get_query_var('paged')) {
            $paged = get_query_var('paged');
        } elseif (get_query_var('page')) {
            $paged = get_query_var('page');
        } else {
            $paged = 1;
        }
        $post_id    = get_the_ID();
        $blog_query = new WP_Query(array('post_type' => 'post', 'paged' => $paged));
        $temp_query = $wp_query;
        $wp_query   = null;
        $wp_query   = $blog_query;

        if ($blog_query->have_posts()) :

            while ($blog_query->have_posts()) : $blog_query->the_post(); ?>
                <?php $postclass = (++$j % 2 == 0) ? 'evenpost' : 'oddpost'; ?>
                <div id="post-<?php the_ID(); ?>" <?php post_class($postclass); ?>>
                    <div class="post-title">
                        <?php get_template_part('post-meta'); ?>
                    </div>


                    <?php
                    $page_id  = get_the_ID();
                    $hasimage = has_post_thumbnail($page_id);
                    if ($hasimage) {
                        echo '<div class="post-image">';
                        echo get_the_post_thumbnail($page_id, 'large', array('class' => 'aligncenter'));
                        echo '</div>';
                    }
                    ?>
                    <?php $postentryclass = $hasimage ? 'image-width' : 'full-width'; ?>
                    <div class="post-entry <?php echo $postentryclass; ?>">


                        <?php the_excerpt(); ?>

                        <?php wp_link_pages(array('before' => '<div class="pagination">' . __('Pages:', 'responsive'), 'after' => '</div>')); ?>
                    </div>
                    <!-- end of .post-entry -->

                    <?php get_template_part('post-data'); ?>

                </div><!-- end of #post-<?php the_ID(); ?> -->


            <?php
            endwhile;

            if ($wp_query->max_num_pages > 1) :
                ?>
                <div class="navigation">
                    <div class="previous"><?php next_posts_link(__('&#8249; Older posts', 'responsive'), $wp_query->max_num_pages); ?></div>
                    <div class="next"><?php previous_posts_link(__('Newer posts &#8250;', 'responsive'), $wp_query->max_num_pages); ?></div>
                </div><!-- end of .navigation -->
            <?php
            endif;

        else :

            get_template_part('loop-no-posts');

        endif;
        $wp_query = $temp_query;
        wp_reset_postdata();
        ?>

    </div><!-- end of #content-blog -->

<?php

get_sidebar();

get_footer();

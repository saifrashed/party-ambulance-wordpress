<?php
// Exit if accessed directly
if (!defined('ABSPATH')) {
    exit;
}

/**
 * Review Widget Template
 *
 *
 * @file           sidebar-review-widget.php
 * @package        Het online recept
 * @author         Sivard Donkers
 * @copyright      2015 Sivard.nl
 * @license        license.txt
 * @version        Release: 1.0
 * @filesource     wp-content/themes/het-online-recept/sidebar-review-widget.php
 * @link           http://codex.wordpress.org/Theme_Development#Widgets_.28sidebar.php.29
 * @since          available since Release 1.1
 */
?>
<?php
if (!is_active_sidebar('blog-widget')
) {
    return;
}
?>
<?php responsive_widgets_before(); // above widgets container hook  ?>

<?php responsive_widgets(); // above widgets hook  ?>

<?php if (is_active_sidebar('blog-widget')) : ?>
    <div id="widgets" class="grid col-300 fit">
    <?php dynamic_sidebar('blog-widget'); ?>
    </div>
    <?php endif; //end of review-widget ?>

<?php responsive_widgets_end(); // after widgets hook ?>

<?php
responsive_widgets_after(); // after widgets container hook ?>
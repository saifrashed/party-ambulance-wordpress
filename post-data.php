<?php

defined('ABSPATH') || exit;

/**
 * Post Data Template-Part File
 */

if (!is_page() && !is_search()) {
    $show_tags = false;
    if (is_single()) {
        $show_tags = responsive_pro_get_option('single_byline_tags');
    } elseif (is_archive()) {
        $show_tags = responsive_pro_get_option('archive_byline_tags');
    } else {
        $show_tags = responsive_pro_get_option('blog_byline_tags');
    }
    $show_categories = false;
    if (is_single()) {
        $show_categories = responsive_pro_get_option('single_byline_categories');
    } elseif (is_archive()) {
        $show_categories = responsive_pro_get_option('archive_byline_categories');
    } else {
        $show_categories = responsive_pro_get_option('blog_byline_categories');
    }
    $show_author_bio = responsive_pro_get_option('single_byline_author_bio');
    if ($show_tags || $show_categories || $show_author_bio) {
        ?>
        <div class="post-data">
            <?php if ($show_categories) {
                responsive_pro_posted_in();
                ?>
                <br/>
                <?php
            }
            if ($show_tags) {
                responsive_pro_post_tags();
                ?>
                <br/>
            <?php } ?>
            <?php responsive_pro_post_author_bio(); ?>
        </div><!-- end of .post-data -->

        <?php
    }
}
?>

<?php edit_post_link(__('Edit', 'responsive'), '<div class="post-edit">', '</div>'); ?>
<?php

defined('ABSPATH') || exit;

/**
 * Functions
 */

require_once __DIR__ . '/functions_woocommerce.php';
require_once __DIR__ . '/functions_custom.php';
require_once __DIR__ . '/core/classes/widget-pagelist.php';
require_once __DIR__ . '/core/classes/simple_html_dom.php';
require_once __DIR__ . '/core/classes/func.php';

// Improve security:
// remove wp version from header.
remove_action('wp_head', 'wp_generator');
/* disable standard api calls */
//remove_action('init', '_add_extra_api_post_type_arguments', 11);
//remove_action('init', '_add_extra_api_taxonomy_arguments', 11);
//// Remove all rest standard routes
//add_filter('rest_endpoints', function ($endpoints) {
//    return array();
//});

// Add theme support
if (function_exists('add_theme_support')) {
    add_theme_support('post-thumbnails');
    set_post_thumbnail_size(200, 200, true);
}

// Thumbnail into post link
add_filter('post_thumbnail_html', function ($html, $post_id) {
    return sprintf('<a href="%s" title="%s">%s</a>', get_permalink($post_id), esc_attr(get_the_title($post_id)), $html);
}, 10, 3);

// add excerpt to pages
add_post_type_support('page', 'excerpt');

// init
add_action('init', function () {

    register_nav_menus(array('footerbottom-menu' => 'Footer bottom menu'));

    // remove styling from parent theme
    remove_action('wp_head', 'responsive_customize_styles', 100);
    remove_action('wp_head', 'responsive_customize_styles');
    // remove image sizes responsive pro
    remove_image_size('responsive-100');
    remove_image_size('responsive-150');
    remove_image_size('responsive-200');
    remove_image_size('responsive-300');
    remove_image_size('responsive-400');
    remove_image_size('responsive-450');
    remove_image_size('responsive-500');
    remove_image_size('responsive-600');
    remove_image_size('responsive-700');
    remove_image_size('responsive-800');
    remove_image_size('responsive-900');
    
    
});

// Register widgets
add_action('widgets_init', function () {
    register_widget('BlueMammoth\PageListWidget');
});

add_filter('widget_text', 'do_shortcode');

// enqueue scripts
add_action('wp_enqueue_scripts', function () {

    // Multisite scripts
    if (is_multisite() && strlen(get_option('multisite_menu_names')) > 0) {
        wp_enqueue_script('mulitsite_menu_script', get_stylesheet_directory_uri() . '/core/js/multisite_menu.js', array('jquery'), null, true);
    }

    wp_enqueue_script('flexslider_script', get_stylesheet_directory_uri() . '/core/js/jquery.flexslider-min.js', array('jquery'), null, true);
    wp_enqueue_style('flexslider-style', get_stylesheet_directory_uri() . '/core/css/flexslider.css');
    wp_enqueue_script('main_theme_script', get_stylesheet_directory_uri() . '/core/js/main.js', array('jquery'), null, true);
}, 110);


add_action('wp_enqueue_scripts', function () {
    wp_enqueue_style('hor_font_awesome', get_stylesheet_directory_uri() . '/core/css/font-awesome.min.css', null, '5.4.1');
}, 900, 0);


//[menu name="" menu_class="" theme_location=""]
function print_menu_shortcode($atts, $content = null) {
    extract(shortcode_atts(array('name' => null, 'menu_class' => null, 'theme_location' => null), $atts));
    return wp_nav_menu(array('menu' => $name, 'echo' => false, 'menu_class' => $menu_class, 'theme_location' => $theme_location));
}

add_shortcode('menu', 'print_menu_shortcode');

remove_action('wp_head', 'print_emoji_detection_script', 7);
remove_action('wp_print_styles', 'print_emoji_styles');

// footer menu
if (is_multisite()) {
    add_option('multisite_menu_names', '', '', 'yes');
    add_action('init', 'wds_multisite_menu_names');
    add_action('wp_update_nav_menu', 'reset_menutransient');
}

function wds_multisite_menu_names() {

    if (!is_main_site())
        return;

    $menunames = get_option('multisite_menu_names');
    if (strlen($menunames) == 0)
        return;
    $menunames_array = explode(',', str_replace(" ", "_", $menunames));
    foreach ($menunames_array as $menuname) {
        if (false === ($wds_menu = get_site_transient('wds_' . $menuname))) {
            // start output buffer
            ob_start();


            if (has_nav_menu($menuname, 'responsive')) {
                if ($menuname == 'sub-header-menu') {
                    wp_nav_menu(array(
                            'container'       => 'div',
                            'container_class' => 'main-nav',
                            'fallback_cb'     => 'responsive_fallback_menu',
                            'theme_location'  => $menuname
                        )
                    );
                } else {
                    wp_nav_menu(array(
                            'container'      => '',
                            'fallback_cb'    => false,
                            'menu_class'     => $menuname,
                            'theme_location' => $menuname
                        )
                    );
                }
            }
            // grab the data from the output buffer and put it in our variable
            $wds_menu = ob_get_contents();
            ob_end_clean();

            // Put sidebar into a transient for 4 hours
            set_site_transient('wds_' . $menuname, $wds_menu, 24 * 60 * 60);
        }
    }
}

function reset_menutransient() {
    // Delete our menu transient. This runs when a menu is saved or updated. Only do this if our nonce is passed.    
    if (is_main_site()) {
        $menunames = get_option('multisite_menu_names');
        if (strlen($menunames) == 0)
            return;
        $menunames_array = explode(',', str_replace(" ", "_", $menunames));
        foreach ($menunames_array as $menuname) {
            delete_site_transient('wds_' . $menuname);
        }
        wds_multisite_menu_names();
    }
}

/* ----End MultiSite Menu Section ------ */


add_filter('the_content', function ($content) {
    $newhtml = str_get_html($content);
    if ($newhtml) {
        foreach ($newhtml->find('h1') as $header) {
            if ($header->class) {
                if (strpos($header->class, 'entry-title') === false) {
                    $header->class .= ' entry-title';
                }
            } else {
                $header->class = 'entry-title';
            }
        }
        return $newhtml;
    }
    return $content;
}, 10, 3);

/* Sitemap options */
add_option('hide_posttype_sitemap');

/* Google Tag Manager */
add_option('google_tag_manager');
add_action('responsive_container', 'google_tag_manager');

function google_tag_manager() {
    if (!cc_perm('gtm')) {
        return;
    }
    $tagid = get_option('google_tag_manager');
    if (strlen($tagid) > 0) {
        echo <<< HTML
<noscript><iframe src="//www.googletagmanager.com/ns.html?id={$tagid}" height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<script>!function(e,t,a,n,g){e[n]=e[n]||[],e[n].push({"gtm.start":(new Date).getTime(),event:"gtm.js"});var m=t.getElementsByTagName(a)[0],r=t.createElement(a);r.async=!0,r.src="//www.googletagmanager.com/gtm.js?id={$tagid}",m.parentNode.insertBefore(r,m)}(window,document,"script","dataLayer");</script>
HTML;
    }
}

add_shortcode('most_popular_posts', 'most_popular_posts');

function most_popular_posts($atts) {
    try {
        $query = new WP_Query(array(
            'post_type'      => 'post',
            'posts_per_page' => 5,
            'post_status'    => 'publish',
            'orderby'        => 'meta_value',
            'meta_key'       => 'views',
            'order'          => 'DESC'
        ));
        $html  = '<div class="popular_posts"><h3>' . __('Popular posts', 'responsive') . '</h3>';
        if ($query->have_posts()) {
            $html .= '<ul class="popular_posts">';
            while ($query->have_posts()) {
                $query->the_post();

                $info = array();
                $info[] = esc_html(get_the_date());
//                $views = intval(get_post_meta(get_the_ID(), 'views', true));
//
//                if ($views > 0) {
//                    $info[] = $views . ' x ' . __('viewed', 'responsive');
//                }

                $html .= '<li><a href="' . get_the_permalink() . '">' . get_the_title() . '</a><div class="post-meta">' . implode(' | ', $info) . '</div></li>';
            }
            $html .= '</ul>';
        } else {
            $html .= __('no posts found.', 'responsive');
        }
        return $html . '</div>';
    } catch (Exception $ex) {
        return 'Error in displaying most_popular_posts: ' . $ex->getMessage();
    }
}

/**
 * Returns a formatted excerpt
 *
 * @param $excerpt
 * @param $maxWords
 * @param $delimiter
 * @return bool|string
 */
function setexcerptlength($excerpt, $maxWords, $delimiter) {
    $btn = '<div class="read-more">';
    if (strrpos($excerpt, $btn)) {
        $excerpt = substr($excerpt, 0, strrpos($excerpt, $btn));
    }
    $arrExcerpt = explode(' ', trim($excerpt));
    if (count($arrExcerpt) < $maxWords) {
        return $excerpt;
    } else {
        $newArray = array_slice($arrExcerpt, 0, $maxWords, true);
        $excerpt  = implode(' ', $newArray);
    }
    return $excerpt . $delimiter;
}

/**
 * Debug a variable
 *
 * @return mixed if first parameter is passed it will be returned
 */
function debug() {
    $args   = func_get_args();
    $output = get_func_output('var_dump', $args);
    if (!defined('DOING_AJAX') || !DOING_AJAX) {
        if (function_exists('xdebug_break')) {
            echo $output;
        } else {
            echo '<pre>' . htmlspecialchars($output) . '</pre>';
        }
    }
    return func_get_args()[0] ?? null;
}

/**
 * Run a shortcode by tag
 *
 * @global array $shortcode_tags
 * @param string $tag     tag
 * @param array  $attr    parameters
 * @param string $content content
 * @return mixed
 */
function hor_shortcode(string $tag, array $attr = array(), string $content = '') {
    global $shortcode_tags;
    if (array_key_exists($tag, $shortcode_tags)) {
        return call_user_func($shortcode_tags[$tag], $attr, $content, $tag);
    }
    return false;
}

/**
 * Returns output generated by function
 *
 * @param callable $func
 * @param array    $params
 * @return string
 */
function get_func_output(callable $func, array $params = array()): string {
    ob_start();
    call_user_func_array($func, $params);
    return ob_get_clean();
}

/**
 * Returns true if id has been allowed. Returns true if consentcookie cookie hasn't been set and $optout is set to true.
 *
 * @param string $id
 * @return bool
 */
function cc_perm($id) {

    if (!isset($_COOKIE['consentcookie'])) {
        return true;
    }

    parse_str($_COOKIE['consentcookie'], $consent);

    if (!isset($consent[$id]) || $consent[$id] == 1) {
        return true;
    }

    return false;
}
<?php

defined('ABSPATH') || exit;

/**
 * Loop Header Template-Part File
 */

get_responsive_breadcrumb_lists();

if (is_category() || is_tag() || is_author() || is_date()) {
    ?>
    <h1 class="entry-title title-archive">
        <?php
        if (is_day()) :
            printf(__('Daily Archives: %s', 'responsive'), '<span>' . get_the_date() . '</span>');
        elseif (is_month()) :
            printf(__('Monthly Archives: %s', 'responsive'), '<span>' . get_the_date('F Y') . '</span>');
        elseif (is_year()) :
            printf(__('Yearly Archives: %s', 'responsive'), '<span>' . get_the_date('Y') . '</span>');
        elseif (is_tag()) :
            printf(__('Posts tagged %s', 'responsive'), single_tag_title('', false));
        elseif (is_category()) :
            printf(__('Archive for %s', 'responsive'), single_cat_title('', false));
        elseif (is_author()) :
            $user_id  = get_query_var('author');
            $userdata = get_the_author_meta('display_name', $user_id);
            printf(__('View all posts by %s', 'responsive'), $userdata);
        else :
            _e('Blog Archives', 'responsive');
        endif;
        ?>
    </h1>
    <?php
    // Show an optional term description.
    $term_description = term_description();
    if (!empty($term_description)) {
        printf('<div class="taxonomy-description">%s</div>', $term_description);
    }
} else {
    if ('post' != get_post_type() && 'page' != get_post_type() && !is_single() && !is_search()) {
        ?>
        <h1 class="entry-title title-archive">
            <?php
            $post_object = get_post_type_object(get_post_type());
            //var_dump($post_object);
            printf(__('Archive for %s', 'responsive'), strtolower($post_object->labels->name));
            ?>
        </h1>
        <?php
    }
}

if (is_search()) {
    ?>
    <h1 class="entry-title title-search-results"><?php printf(__('Search results for: %s', 'responsive'), '<span>' . get_search_query() . '</span>'); ?></h1>
    <?php
}
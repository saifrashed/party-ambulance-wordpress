<?php

defined('ABSPATH') || exit;

/**
 * Full Content Template
 *
 * Template Name:  Overzichtspagina (met sidebar)
 */

get_header();

?>
    <div id="content-full" class="grid col-620">
        <?php get_responsive_breadcrumb_lists(); ?>
        <div id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
            <div class="post-entry">
                <?php the_content(); ?>
            </div>
        </div>
        <?php

        $mypages = get_pages(array('child_of' => get_the_ID(), 'sort_order' => 'asc'));

        foreach ($mypages as $page) {
            $content = $page->post_excerpt;
            if (strlen($content) == 0) {
                $content = preg_replace("/\[(.*?)\]/i", '', $page->post_content);
                $content = strip_tags($content);
            }
            if (!$content) // Check for empty page
                continue;
            $content = apply_filters('the_content', $content);
            ?>
            <div id="post-<?php echo $page->ID; ?>" <?php post_class(); ?>>
                <div class="title">
                    <h2><a href="<?php echo get_page_link($page->ID); ?>"
                           rel="bookmark"><?php echo $page->post_title; ?></a></h2>
                </div>
                <div class="post-entry">
                    <?php
                    if (has_post_thumbnail($page->ID)) {
                        echo get_the_post_thumbnail($page->ID, 'large', array('class' => 'aligncenter'));
                    } else {
                        echo get_the_post_thumbnail($post_id, 'large', array('class' => 'aligncenter'));
                    } ?>
                    <p><?php
                        if (strpos(substr($content, 0, 400), ".") > -1) {
                            echo substr($content, 0, strrpos(substr($content, 0, 400), ".") + 1) . ' ';
                        } else {
                            echo substr($content, 0, strrpos(substr($content, 0, 400), " ")) . '<br/>';
                        }
                        ?>
                    </p></div>
                <div class="read-more">
                    <a class="moretag" href="<?php echo get_permalink($page->ID) ?>">Lees meer...</a>
                </div>
            </div>
        <?php }
        ?>
    </div><!-- end of #content-full -->
<?php

get_sidebar('right');

get_footer();



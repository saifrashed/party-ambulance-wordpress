<?php

defined('ABSPATH') || exit;

/**
 * Functions custom
 */
$hor_backgroundimage = '/wp-content/themes/het-online-recept/core/icons/hor_block_placeholder.jpg';

function responsive_pro_posted_on() {
    // Get value of post byline date toggle option from theme option for different pages
    if (is_single()) {
        $show_date = responsive_pro_get_option('single_byline_date');
    } elseif (is_archive()) {
        $show_date = responsive_pro_get_option('archive_byline_date');
    } else {
        $show_date = responsive_pro_get_option('blog_byline_date');
    }
    $posted_on = sprintf(__('<span class="%1$s">Posted on </span>%2$s', 'responsive'), 'meta-prep meta-prep-author posted', sprintf('<a href="%1$s" title="%2$s" rel="bookmark"><span class="timestamp updated">%3$s</span></a></span>', esc_url(get_permalink()), esc_attr(get_the_time()), esc_html(get_the_date())));
    // If post byline date toggle is on then print HTML for date link.
    if ($show_date) {
        echo $posted_on;
    }
}

// function to register template widget  
add_action('widgets_init', 'register_blogdetail_widget');

function register_blogdetail_widget() {
    register_sidebar(array(
        'name' => 'blog detail widget',
        'id' => 'blog-widget',
        'description' => 'Widget for blog detail page',
        'before_widget' => '',
        'after_widget' => '',
        'before_title' => '<div class="widgettitle">',
        'after_title' => '</div>',
    ));
    register_sidebar(array(
        'name' => 'Woocommerce Shop Widget',
        'id' => 'shop-widget',
        'description' => 'Widget for shop page',
        'before_widget' => '',
        'after_widget' => '',
        'before_title' => '<div class="widgettitle">',
        'after_title' => '</div>',
    ));
}

function hor_blog_author_widget() {
    try {
        $backgroundimage = '/wp-content/uploads/2017/09/Blue_Mammoth_Logo_PNG_Embleem.png';
        $author_id = get_the_author_meta('ID');
        $authorimage = get_wp_user_avatar($author_id, 'medium');
        if ($authorimage == '') {
            $authorimage = '<img src="' . $backgroundimage . '"/>';
        }
        $authorhtml = esc_html(get_the_author());
        return <<< HTML
<div class="author_block">
    <div class="author_image">$authorimage</div>
    <div class="author_details">            
    <div class="author_text"><p><small>Geschreven door: </small></p></div>
    <div class="author_name">$authorhtml</div>
    </div>
</div>
HTML;
    } catch (Exception $ex) {
        return 'Error in displaying mailplus_blog_author_widget: ' . $ex->getMessage();
    }
}

add_shortcode('hor_blog_author_widget', 'hor_blog_author_widget');

add_filter('blokkenmodule_noposts', function ($string, $data) {
    $posttype = get_post_type_object($data['load_from']);
    $text = sprintf('Geen %s gevonden', $posttype->name);
    return <<< HTML
    <li class="noposts">
        <div class="icon"><i class="fa fa-frown-o"></i></div>
        <div class="text">{$text}</div>
    </li>
HTML;
}, 10, 2);

function horblock_publications($post, array $values): string {
    $readmore = __('Read more &#8250;', 'responsive');
    return <<< HTML
    <li class="blog-list" data-cat="{$values['cat']}">
        <div id="post-{$post->ID}">
            <a href="{$values['permalink']}">
                <div class="post-image" style="background-image:url({$values['backgroundimage']});">&nbsp;</div>
            </a>
            <div class="post-entry">
                <{$values['headertag']} class="block-title"><a href="{$values['permalink']}">{$post->post_title}</a></{$values['headertag']}>
                <div class="post-excerpt">{$values['excerpt']}</div>
            </div>
            <div class="blogreadmore"><a class="button" href="{$values['permalink']}">{$readmore}</a></div>
        </div>
    </li>
HTML;
}

function horblock_nieuws($post, array $values): string {
    $info = array();
    $info[] = get_the_date('d-m-Y', $post->ID);
    $cat = get_post_meta($post->ID, '_category_permalink', true);
    if ($cat != null) {
        $category = get_category($cat['category']);
    } else {
        $categories = get_the_category($post->ID);
        $category = isset($categories[0]) ? $categories[0] : false;
    }
    $meta = esc_html(implode(' | ', array_filter($info)));
    $readmore = __('Read more &#8250;', 'responsive');
    return <<< HTML
    <li class="blog-list" data-cat="{$values['cat']}">
        <div id="post-{$post->ID}">
            <a href="{$values['permalink']}">
                <div class="post-image" style="background-image:url({$values['backgroundimage']});">&nbsp;</div>
            </a>
            <div class="post-entry">
                <{$values['headertag']} class="block-title"><a href="{$values['permalink']}">{$post->post_title}</a></{$values['headertag']}>
                <div class="post-meta">{$meta}</div>
                <div class="post-excerpt">{$values['excerpt']}</div>
            </div>
            <div class="blogreadmore"><a class="button" href="{$values['permalink']}">{$readmore}<i class="fas fa-chevron-right"></i></a></div>
        </div>
    </li>
HTML;
}

function horblock_infograph($post, array $values): string {
    $readmore = __('Read more &#8250;', 'responsive');
    return <<< HTML
      <li class="blog-list" data-cat="{$values['cat']}">
        <div id="post-{$post->ID}">
            <a href="{$values['permalink']}">
                <div class="post-image" style="background-image:url({$values['backgroundimage']});">&nbsp;</div>
            </a>
            <div class="post-entry">
                <{$values['headertag']} class="block-title"><a href="{$values['permalink']}">{$post->post_title}</a></{$values['headertag']}>
                  <div class="blogreadmore"><a class="button" href="{$values['permalink']}">{$readmore}</a></div>
            </div>
          
        </div>
    </li>
HTML;
}

function horblock_diensten_custom($post, $values) {
    $html = '';
    $video = get_field('youtube_id', $post->ID);
    $bekijk = __('View service', 'responsive');
    if ($video == '') {
        $html .= <<< HTML
<li class="block_post_{$post->ID}" data-cat="{$values['cat']}">
    <div class="block_container">
        <a href="{$values['permalink']}"><div class="post-image" style="background-image:url({$values['backgroundimage']});"></div></a>
        <div class="post-entry"><{$values['headertag']} class="block-title"><a class="entry-title post-title" href="{$values['permalink']}">{$post->post_title}</a></{$values['headertag']}>
        <div class="post-excerpt">{$values['excerpt']}</div></div>
    </div>
        <div class="blogreadmore"><a class="button" href="{$values['permalink']}">{$bekijk}</a></div>
</li>           
HTML;
    } else {
        $html .= <<< HTML
<li class="block_post_{$post->ID}" data-cat="{$values['cat']}">
    <div class="block_container">
        <div class="youtube" data-embed="{$video}"><div class="play-button"></div></div>
        <div class="post-entry"><{$values['headertag']} class="block-title"><a class="entry-title post-title" href="{$values['permalink']}">{$post->post_title}</a></{$values['headertag']}>
        <div class="post-excerpt">{$values['excerpt']}</div></div>
    </div>
        <div class="blogreadmore"><a class="button" href="{$values['permalink']}">{$bekijk}</a></div>
</li>           
HTML;
    }
    return $html;
}
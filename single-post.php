<?php
defined('ABSPATH') || exit;

/**
 * Single Posts Template
 */
get_header();
?>
<div id="content" class="<?php echo implode(' ', responsive_get_content_classes()); ?>">
<?php get_responsive_breadcrumb_lists(); ?>
<?php responsive_entry_before(); ?>
    <div id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
    <?php responsive_entry_top(); ?>
        <div class="post-entry blogshadow">

        <?php
        $pageid          = get_the_ID();
        $hasimage        = has_post_thumbnail();
        $backgroundimage = $hor_backgroundimage;
        $alt_text        = get_the_title();
        $alt_title       = $alt_text;
        if ($hasimage) {
            $attachment_id = get_post_thumbnail_id($page_id);
            $image         = wp_get_attachment_image_src($attachment_id, 'large');
            $alt_text      = trim(strip_tags(get_post_meta($attachment_id, '_wp_attachment_image_alt', true)));
            $alt_title     = get_post($attachment_id)->post_title;
            if (count($image) > 0 && $image[0]) {
                $backgroundimage = $image[0];
            }
        }
        ?> 
            <div class="post-image">
                <img title="<?php echo $alt_title; ?>" alt="<?php echo $alt_text; ?>" src="<?php echo $backgroundimage ?>"/>
            </div>   
            <div class="post-text">
<?php if (strpos(get_the_content(), '</h1>') === false) { ?>
                    <h1 class="post-title"><?php the_title() ?></h1>
<?php } ?>
                <?php the_content(__('Read more &#8250;', 'responsive')); ?>
            </div>
        </div><!-- end of .post-entry -->

        <div class="navigation">
            <div class="previous"><?php previous_post_link('%link', '<i class="fa fa-chevron-left"></i> ' . __('Previous post', 'responsive')); ?></div>
            <div class="next"><?php next_post_link('%link', __('Next post', 'responsive') . ' <i class="fa fa-chevron-right"></i>'); ?></div>
        </div>

<?php responsive_entry_bottom(); ?>
    </div><!-- end of #post-<?php the_ID(); ?> -->
<?php responsive_entry_after(); ?>

</div><!-- end of #content -->

    <?php
    get_sidebar('blog');

    get_footer();
    
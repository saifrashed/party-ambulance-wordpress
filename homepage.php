<?php

defined('ABSPATH') || exit;

/**
 * Full Content Template
 *
 * Template Name:  Home Pagina (zonder sidebar)
 */

get_header();

?>
<div id="content-full" class="grid col-940">
    <div id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
        <div class="post-entry">
            <?php the_content(); ?>
        </div>
    </div><!-- end of #post-<?php the_ID(); ?> -->
</div><!-- end of #content-full -->
<?php

get_footer();

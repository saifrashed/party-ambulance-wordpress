<?php

defined('ABSPATH') || exit;

/**
 * Search Form Template
 */

?>
<form method="get" id="searchform" action="<?php echo home_url( '/' ); ?>">
    <span class="inputsearch_area">
        <input type="text"  aria-label="search field" class="field" name="s" id="s" placeholder="<?php esc_attr_e( 'search here &hellip;', 'responsive' ); ?>" /> 
    </span>
    <button name="submit" aria-label="search" class="submit" id="searchsubmit" onclick="return checksearchinput(this.form);"><i class="fa fa-search"></i></button>	   
    
</form>
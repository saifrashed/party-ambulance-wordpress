<?php

defined('ABSPATH') || exit;

/**
 * Loop Navigation Template-Part File
 */

if ($wp_query->max_num_pages > 1) {

    echo '<div class="navigation">';

    the_posts_pagination(array(
        'mid_size'  => 5,
        'prev_text' => __('&#8249; Older posts', 'responsive'),
        'next_text' => __('Newer posts &#8250;', 'responsive'),
    ));

    echo '</div><!-- end of .navigation -->';

}
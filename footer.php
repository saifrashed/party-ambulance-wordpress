<?php

defined('ABSPATH') || exit;

/**
 * Footer Template
 */

global $responsive_options;
$responsive_options = responsive_get_options();

responsive_wrapper_bottom(); ?>
</div><!-- end of #wrapper -->
<?php responsive_wrapper_end(); ?>
</div><!-- end of #container -->
<?php responsive_container_end(); ?>

<div id="footertopcontainer" class="clearfix">
    <div id="container">
        <?php get_sidebar('colophon'); ?>
    </div>
</div><!-- end #footertopcontainer -->
<?php responsive_footer_after(); ?>

<?php if (basename(get_page_template()) != 'homepage.php') { ?>
    <div id="footermiddlecontainer" class="clearfix">
        <div id="container">
            <div class="grid col-940"><?php
                if (is_multisite() && strpos(get_option('multisite_menu_names'), 'header-menu') !== false) {
                    $show_header_menu = get_site_transient('wds_header-menu');
                    echo $show_header_menu;
                } else {
                    if (has_nav_menu('header-menu')) {
                        wp_nav_menu(array(
                            'container'      => '',
                            'fallback_cb'    => false,
                            'menu_class'     => 'header-menu',
                            'theme_location' => 'header-menu'
                        ));
                    }
                    ?>


                <?php } ?>
            </div>
        </div>
    </div>
<?php } ?>
<div id="footerbottomcontainer" class="clearfix">
    <div id="container">
        <?php responsive_footer_top(); ?>
        <div class="grid col-940">
            <?php
            if (is_multisite() && strpos(get_option('multisite_menu_names'), 'footer-menu') !== false) {
                $show_footer_menu = get_site_transient('wds_footer-menu');
                echo $show_footer_menu;
            } else {
                if (has_nav_menu('footer-menu')) {
                    wp_nav_menu(array(
                        'container'      => '',
                        'fallback_cb'    => false,
                        'menu_class'     => 'footer-menu',
                        'theme_location' => 'footer-menu'
                    ));
                }
            }
            ?>

            <!-- end of col-940 -->
        </div>
        <?php responsive_footer_bottom(); ?>
        <?php get_sidebar('footer'); ?>

    </div> <!-- end container -->
</div> <!-- end footerbottomcontainer -->
<?php if (has_nav_menu('footerbottom-menu')) { ?>
    <div id="footerbottommenucontainer" class="clearfix">
        <div id="container">

            <?php
            wp_nav_menu(array(
                    'container'      => '',
                    'fallback_cb'    => false,
                    'menu_class'     => 'footerbottom-menu',
                    'theme_location' => 'footerbottom-menu'
                )
            );
            ?>
        </div> <!-- end container -->
    </div> <!-- end footerbottommenucontainer -->

<?php } ?>

<?php wp_footer(); ?>
</div><!-- end of fullcontainer -->
</body>
</html>
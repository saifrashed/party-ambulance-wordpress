<?php

defined('ABSPATH') || exit;

/**
 * Single Posts Template
 */

get_header();

?>

    <div id="content" class="grid">

        <?php get_template_part('loop-header'); ?>

        <?php if (have_posts()) : ?>

            <?php while (have_posts()) : the_post(); ?>

                <?php responsive_entry_before(); ?>
                <div id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
                    <?php responsive_entry_top(); ?>

                    <?php //get_template_part('post-meta'); ?>

                    <div class="post-entry">
                        <div class="teammember blogshadow">
                            <div class="teammember-content">
                                <h1><?php the_title(); ?></h1>
                                <div class="post-content">
                                    <?php the_content(__('Read more &#8250;', 'responsive')); ?>
                                </div>
                            </div>
                            <div class="teammember-image">
                                <?php
                                $pageid          = get_the_ID();
                                $hasimage        = has_post_thumbnail($page_id);
                                $backgroundimage = '/wp-content/themes/het-online-recept/icons/hor_block_placeholder.jpg';
                                if ($hasimage) {
                                    $image = wp_get_attachment_image_src(get_post_thumbnail_id($page_id), 'medium');
                                    if (count($image) > 0 && $image[0]) {
                                        $backgroundimage = $image[0];
                                    }
                                }
                                ?>
                                <div class="post-image">
                                    <img src="<?php echo $backgroundimage ?>"/>
                                </div>
                                <div class="post-social">
                                    <?php
                                    $functie    = get_field('functie_omschrijving');
                                    $socialdata = '';
                                    while (have_rows('social_media_links')): the_row();
                                        $type = get_sub_field('soort_link');
                                        $link = get_sub_field('link');
                                        switch ($type) {
                                            case 'email':
                                                $socialdata .= '<li class="socialicons-li"><a href="mailto:' . $link . '"><i class="fa fa-envelope" aria-hidden="true"></i>' . $link . '</a></li>';
                                                break;
                                            case 'telefoon':
                                                $socialdata .= '<li class="socialicons-li"><a href="call:' . $link . '"><i class="fa fa-phone" aria-hidden="true"></i>' . $link . '</a></li>';
                                                break;
                                            case 'linkedin':
                                                $socialdata .= '<li class="socialicons-li"><a href="' . $link . '" target="_blank"><i class="fa fa-linkedin" aria-hidden="true"></i>' . $link . '</a></li>';
                                                break;
                                            case 'twitter':
                                                $socialdata .= '<li class="socialicons-li"><a href="' . $link . '" target="_blank"><i class="fa fa-twitter" aria-hidden="true"></i>' . $link . '</a></li>';
                                                break;
                                            case 'facebook':
                                                $socialdata .= '<li class="socialicons-li"><a href="' . $link . '" target="_blank"><i class="fa fa-facebook-official" aria-hidden="true"></i>' . $link . '</a></li>';
                                                break;
                                            case 'whatsapp':
                                                $socialdata .= '<li class="socialicons-li"><a href="' . $link . '" target="_blank"><i class="fa fa-whatsapp" aria-hidden="true"></i>' . $link . '</a></li>';
                                                break;
                                            case 'instagram':
                                                $socialdata .= '<li class="socialicons-li"><a href="' . $link . '" target="_blank"><i class="fa fa-instagram" aria-hidden="true"></i>' . $link . '</a></li>';
                                                break;
                                            case 'googleplus':
                                                $socialdata .= '<li class="socialicons-li"><a href="' . $link . '" target="_blank"><i class="fa fa-google-plus-square" aria-hidden="true"></i>' . $link . '</a></li>';
                                                break;
                                            default:
                                                $socialdata .= '<li class="socialicons-li"><a href="' . $link . '" target="_blank"><i class="fa fa-internet-explorer" aria-hidden="true"></i>' . $link . '</a></li>';
                                                break;
                                        }
                                    endwhile;
                                    if ($functie) {
                                        echo '<div class="jobdescription">' . $functie . '</div>';
                                    }
                                    echo '<ul class="socialicons-ul">' . $socialdata . '</ul>';
                                    ?>
                                </div>
                            </div>
                        </div>
                        <div class="navigation">
                            <div class="previous"><?php previous_post_link('<i class="fa fa-chevron-left"></i> %link'); ?></div>
                            <div class="next"><?php next_post_link('%link <i class="fa fa-chevron-right"></i>'); ?></div>
                        </div>

                    </div>
                    <!-- end of .post-entry -->


                    <!-- end of .navigation -->

                    <?php get_template_part('post-data'); ?>

                    <?php responsive_entry_bottom(); ?>
                </div><!-- end of #post-<?php the_ID(); ?> -->
                <?php responsive_entry_after(); ?>


            <?php
            endwhile;

            get_template_part('loop-nav');

        else :

            get_template_part('loop-no-posts');

        endif;
        ?>
    </div><!-- end of #content -->
<?php

// get_sidebar();

get_footer();

